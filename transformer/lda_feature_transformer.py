from pyvi.pyvi import ViTokenizer, ViPosTagger
from sklearn.base import TransformerMixin, BaseEstimator
from nltk.corpus import stopwords
import re
import pandas as pd


class LDA_FeatureTransformer(BaseEstimator, TransformerMixin):
    def __init__(self):
        self.tokenizer =  ViTokenizer()
        self.pos_tagger = ViPosTagger()

    def fit(self, *_):
        return self

    def remove_stopwords(text, stopwords):
        return " ".join([word for word in text.split() if word not in stopwords])
    def transform(self, X, y=None, **fit_params):
        result = X.apply(lambda text: self.tokenizer.tokenize(text))
        # Remove punctuation
        result= result.map(lambda x: re.sub('[;,\.!?():]', '', x))
        # Convert the titles to lowercase
        result= result.map(lambda x: x.lower())
        return result
