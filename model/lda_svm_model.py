from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from transformer.lda_feature_transformer import LDA_FeatureTransformer
from sklearn.decomposition import LatentDirichletAllocation as LDA
from sklearn.linear_model import SGDClassifier
from sklearn. svm import SVC
# https://scikit-learn.org/stable/modules/classes.html#module-sklearn.svm

class LDASVMModel(object):
    def __init__(self):
        self.clf = self._init_pipeline()

    @staticmethod
    def _init_pipeline():
        pipe_line = Pipeline([
            ("transformer", LDA_FeatureTransformer()), #sử dụng pyvi tiến hành word segmentation
            ("vect", CountVectorizer()),##bag-of-words, CountVectorizer để cài đặt các ánh xạ từ các từ sang các phần tử của vector. Nó thu được cả các đặc trưng và các đầu ra mong đợi.
            ("lda", LDA(n_components=64)),  ##lda
            ("clf-svm",SVC(kernel='linear', degree=2, gamma='scale', coef0=0.0, shrinking=True, probability=False, tol=0.001, cache_size=200, class_weight=None, verbose=False, max_iter=-1, decision_function_shape='ovr', random_state=None)) # best hyperparameter
        ])
        return pipe_line