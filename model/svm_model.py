from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from transformer.feature_transformer import FeatureTransformer
from sklearn.linear_model import SGDClassifier
from sklearn. svm import SVC



class SVMModel(object):
    def __init__(self):
        self.clf = self._init_pipeline()

    @staticmethod
    def _init_pipeline():
        pipe_line = Pipeline([
            ("transformer", FeatureTransformer()), #sử dụng pyvi tiến hành word segmentation
            ("vect", CountVectorizer()),##bag-of-words, CountVectorizer để cài đặt các ánh xạ từ các từ sang các phần tử của vector. Nó thu được cả các đặc trưng và các đầu ra mong đợi.
            ("tfidf", TfidfTransformer()), ##tf-idf
            ("clf-svm",SVC(kernel='linear', degree=2, gamma='scale', coef0=0.0, shrinking=True, probability=False, tol=0.001, cache_size=200,
                           class_weight=None, verbose=False, max_iter=-1, decision_function_shape='ovr', random_state=None)) # best hyperparameter
            # ("clf-svm",SVC(kernel='sigmoid', degree=2, gamma='scale', coef0=0.0, shrinking=True, probability=False, tol=0.001, cache_size=200, class_weight=None, verbose=False, max_iter=-1, decision_function_shape='ovr', random_state=None))
            #("clf-svm", SGDClassifier(loss='log', penalty='l2', alpha=1e-3, random_state=None)) #model svm, https://scikit-learn.org/stable/modules/generated/sklearn.svm.SVC.html
        ])

        return pipe_line