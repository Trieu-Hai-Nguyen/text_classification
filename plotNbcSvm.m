clc
clear
close all
% set(groot,'defaulttextinterpreter','latex'); 
% set(groot, 'defaultAxesTickLabelInterpreter','latex'); 
% set(groot, 'defaultLegendInterpreter','latex'); 
x=[413 516 688 1032 2064];
svm_linear=[84.81 85.38 86.05 88.08 92.13];
svm_rbf=[80.88 83.46 85.38 87.51 90.78];
svm_poly=[79.30 82.00 84.59 86.50 89.99];
svm_sigmoid=[84.25 83.80 84.70 87.40 90.21];
nbc_1=[75.93 78.63 80.2 81.55 84.59];
nbc_05=[81.21 83.01 83.80 84.81 87.63];
nbc_00001=[84.93 85.60 86.73 88.08 90.10];
plot (x,svm_linear,x,svm_rbf,x,svm_poly,x,svm_sigmoid,'LineWidth',1.2)
set(gca,'FontSize',11)
xlabel('Number of training data','interpreter','latex');
ylabel('Accuracy \%','interpreter','latex');
legend('Linear Kernel','Gaussian RBF','Polynomial Kernel','Sigmoid Kernel','interpreter','latex',...
    'Location','southeast')
figure
plot(x,nbc_00001,x,nbc_05,x,nbc_1,'LineWidth',1.2)
set(gca,'FontSize',11)
xlabel('Number of training data','interpreter','latex');
ylabel('Accuracy \%','interpreter','latex');
legend('$\alpha=10^{-4}$','$\alpha=0.5$','$\alpha=1$','interpreter','latex',...
    'Location','southeast')
