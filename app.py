#!/usr/bin/env python
# -*- coding: utf-8 -*-
# from tkinter import *
import tkinter as tk
from tkinter import messagebox
from tkinter.filedialog import askopenfilename
from db import Database
from pyvi.pyvi import ViTokenizer, ViPosTagger
import pandas as pd
from model.svm_model import SVMModel
from model.naive_bayes_model import NaiveBayesModel
from sklearn.metrics import accuracy_score # for evaluating results
import csv, os.path, pathlib
from sklearn.externals import joblib
import matplotlib.pyplot as plt
from model.lda_svm_model import LDASVMModel
import numpy as np


db = Database('store.db')
class TextClassificationPredict(object):
    def __init__(self):
        self.test = None

    def get_train_data(self, trainingdata):
        train_data = []
        for i in range(len(trainingdata)):
            train_data.append({"feature": trainingdata['training data'][i], "target": trainingdata['label'][i]})
        df_train = pd.DataFrame(train_data)
        return  df_train

    def get_test_data(self, testset):
        test_data = []
        try:
            for i in range(len(testset)):
                test_data.append({"feature":testset['test'][i], "target": testset['label'][i]})
            df_test = pd.DataFrame(test_data)
            return df_test
        except:
            for i in range(len(testset)):
                test_data.append({"feature":testset['test'][i]})
            df_test = pd.DataFrame(test_data)
            return df_test
    def process_example(self, df_train, df_test, file_name):
        model = SVMModel()
        try:
            clf = joblib.load('./save_model/one_example_%s.pkl'%file_name)
            print ("using trained model")
        except:
            clf = model.clf.fit(df_train["feature"], df_train.target)
            model_path = os.path.join('./save_model', 'one_example_%s.pkl'%file_name)
            os.makedirs(os.path.dirname(model_path), exist_ok=True)
            joblib.dump(clf, model_path)
            print ("building new model")
        predicted = clf.predict(df_test["feature"])
        return predicted[0]
    def process_classification(self, df_train, df_test, file_name):
        # init model naive bayes
        model = SVMModel()
        # model = NaiveBayesModel()
        # model=LDASVMModel()
        try:
            clf = joblib.load('./save_model/svm_model_%s.pkl'%file_name) #lda_svm_model_%s.pkl
            print ("using trained model")
        except:
            clf = model.clf.fit(df_train["feature"], df_train.target)
            model_path = os.path.join('./save_model', 'svm_model_%s.pkl'%file_name) #lda_svm_model_%s.pkl
            os.makedirs(os.path.dirname(model_path), exist_ok=True)
            joblib.dump(clf, model_path)
            print("building new model")
        clf = model.clf.fit(df_train["feature"], df_train.target)
        predicted = clf.predict(df_test["feature"])
        df_test['prediction'] = predicted # update result to dataframe df_test
        # update label to here
        test_label=df_test.get('target')
        # get values from frame
        df_test_predindex=df_test.set_index('prediction')
            #for classes:
        pre_phuong_phap = df_test_predindex.loc['phương pháp','feature'].values
        pre_thai_do = df_test_predindex.loc['thái độ', 'feature'].values
        pre_co_so = df_test_predindex.loc['cơ sở vật chất', 'feature'].values
        # pre_khac = df_test_predindex.loc['khác', 'feature'].values
        try:
            pre_khac = df_test_predindex.loc['khác', 'feature'].values
        except:
            pre_khac=np.empty(1)
        #create new folder: mkdir
        new_dir_name='OUTPUT'
        new_dir = pathlib.Path('./', new_dir_name)
        # write to .CSV file
        csvlogPath = os.path.join(new_dir, 'Phuong_Phap_Giang_Day.csv')
        os.makedirs(os.path.dirname(csvlogPath), exist_ok=True)
        pd.DataFrame(pre_phuong_phap).to_csv(csvlogPath, header=None) #index=None
        csvlogPath = os.path.join(new_dir, 'Thai_Do_GV.csv')
        os.makedirs(os.path.dirname(csvlogPath), exist_ok=True)
        pd.DataFrame(pre_thai_do).to_csv(csvlogPath, header=None)  # index=None
        csvlogPath = os.path.join(new_dir, 'Co_So_Vat_Chat.csv')
        os.makedirs(os.path.dirname(csvlogPath), exist_ok=True)
        pd.DataFrame(pre_co_so).to_csv(csvlogPath, header=None)  # index=None
        csvlogPath = os.path.join(new_dir, 'Y_Kien_Khac.csv')
        os.makedirs(os.path.dirname(csvlogPath), exist_ok=True)
        pd.DataFrame(pre_khac).to_csv(csvlogPath, header=None)  # index=None
        # with open(csvlogPath, 'w', newline='') as file:
        #     writer = csv.writer(file)
        #     writer.writerows(pre_phuong_phap)
            # for i in range(len(predicted)):
            #     if predicted[i]== 'phương pháp':
            #         writer.writerow([df_test.at[i,'feature']])
        is_empty = test_label.values.size == 0 #test_label[0]==''
        # Detect existing of test label
        if is_empty:
            pass
        else:
            return accuracy_score(test_label, predicted)*100
    def proccess_result(self):
        phuong_phap = pd.read_csv('./OUTPUT/Phuong_Phap_Giang_Day.csv')
        thai_do = pd.read_csv('./OUTPUT/Thai_Do_GV.csv')
        co_so = pd.read_csv('./OUTPUT/Co_So_Vat_Chat.csv')
        khac = pd.read_csv('./OUTPUT/Y_Kien_Khac.csv')
        n_pp=len(phuong_phap); n_td=len(thai_do); n_cs=len(co_so); n_k=len(khac)
        n_sum=n_pp+n_td+n_cs+n_k
        labels = 'Phương pháp giảng dạy', 'Thái độ GV', 'Cơ sở vật chất', 'Ý kiến khác'
        sizes = [n_pp, n_td, n_cs, n_k]
        sizes[:]=["{:0.2f}".format(x*100/n_sum) for x in sizes]
        ##### using matplotlib, comment it if have errors
        explode = (0.1, 0, 0, 0)  # only "explode" the 2nd slice (i.e. 'Hogs')
        fig1, ax1 = plt.subplots()
        ax1.pie(sizes, explode=explode, labels=labels, autopct='%1.1f%%',
                shadow=True, startangle=90)
        ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.
        plt.show()
        #####
        return sizes



class Application(tk.Frame):
    def __init__(self, master):
        super().__init__(master)
        self.master = master
        master.title('Phân Loại Ý Kiến Người Học-ĐHNT')
        # Width height
        master.geometry("600x385")
        # master.resizable(width=False, height=False)
        # Create widgets/grid
        self.create_widgets()
        # Init selected item var
        self.selected_item = 0
        # Populate initial list
        self.populate_list()
        # global variables
        self.data_of_trainset = self.data_of_testset = pd.DataFrame()
    def create_widgets(self):
        # input
            # by file
        self.input_train_text=tk.StringVar()
        self.input_train_label=tk.Label(self.master,text='Input files:')
        self.input_train_label.config(font=("Times New Roman", 14, "bold"))
        self.input_train_label.grid(row=0, column=0,pady=20)
            # by example
        self.input_exam_text=tk.StringVar()
        self.input_exam_label=tk.Label(self.master,text='Input an Example:')
        self.input_exam_label.config(font=("Times New Roman", 14, "bold"))
        self.input_exam_label.grid(row=1, column=0,pady=5)
        self.input_exam_entry = tk.Entry(self.master, textvariable=self.input_exam_text, width=50)
        self.input_exam_entry.grid(row=1, column=1,columnspan=3)

        # Buttons
            # train file
        self.trainfile_btn = tk.Button(self.master, text="Traning File", width=12,command=self.add_file_training)
        self.trainfile_btn.config(font=("Times New Roman", 12))
        self.trainfile_btn.grid(row=0, column=1)
            # test file
        self.testfile_btn = tk.Button(self.master, text="Test File", width=12,command=self.add_file_test)
        self.testfile_btn.config(font=("Times New Roman", 12))
        self.testfile_btn.grid(row=0, column=2)
            # predict example
        self.pre_exam_btn = tk.Button(self.master, text="Predict Example", width=12,command=self.add_item)
        self.pre_exam_btn.config(font=("Times New Roman", 12))
        self.pre_exam_btn.grid(row=2, column=0, pady=20)
            # predict testfile
        self.pre_test_btn = tk.Button(self.master, text="Predict Test File", width=12,command=self.predict_test_file)
        self.pre_test_btn.config(font=("Times New Roman", 12))
        self.pre_test_btn.grid(row=2, column=1, pady=20)
            # predict static
        self.pre_static_btn = tk.Button(self.master, text="Static", width=12,command=self.analyze_result)
        self.pre_static_btn.config(font=("Times New Roman", 12))
        self.pre_static_btn.grid(row=2, column=2, pady=20)
            # predict save
        self.pre_save_btn = tk.Button(self.master, text="Save Prediction", width=12,command=self.save_classification)
        self.pre_save_btn.config(font=("Times New Roman", 12))
        self.pre_save_btn.grid(row=2, column=3, pady=20)
        #footer
        self.footer_text=tk.StringVar()
        self.footer_label=tk.Label(self.master,text='©Trường Đại Học Nha Trang',
                                   bd=1,relief=tk.SUNKEN, anchor=tk.W)
        self.footer_label.config(font=("Times New Roman", 11, "bold"))
        self.footer_label.grid(row=14,column=0, columnspan=10)

        # Parts list (listbox)
        self.parts_list = tk.Listbox(self.master, height=8, width=50, border=0)
        self.parts_list.grid(row=3, column=1, columnspan=4,
                             rowspan=10, pady=20, sticky=tk.W)
        # Create scrollbar
        self.scrollbar = tk.Scrollbar(self.master)
        self.scrollbar.grid(row=8, column=4, pady=20, sticky=tk.E)
        # Set scrollbar to parts
        self.parts_list.configure(yscrollcommand=self.scrollbar.set)
        self.scrollbar.configure(command=self.parts_list.yview)

        # Bind select
        self.parts_list.bind('<<ListboxSelect>>', self.select_item)

    # add file
    def add_file_training(self):
        try:
            train_file_path=askopenfilename(title='Select a training file',filetypes=[('.csvfiles', '.csv')])
            self.train_file_name=os.path.basename(train_file_path).replace('.csv','')
            trainingdata=pd.read_csv(train_file_path)
            tcp = TextClassificationPredict()
            self.data_of_trainset=tcp.get_train_data(trainingdata)
        except:
            tk.messagebox.showwarning('Error!!!', 'Did not input training file')

    def add_file_test(self):
        try:
            test_file_path=askopenfilename(title='Select a test file',filetypes=[('.csvfiles', '.csv')])
            self.test_file_name=os.path.basename(test_file_path).replace('.csv','')
            testdata=pd.read_csv(test_file_path)
            tcp = TextClassificationPredict()
            self.data_of_testset=tcp.get_test_data(testdata)
        except:
            tk.messagebox.showwarning('Error!!!', 'Did not input test file')
    # predict_test_file
    def predict_test_file(self):
        try:
            tcp=TextClassificationPredict()
            self.accuracy=tcp.process_classification(self.data_of_trainset, self.data_of_testset, self.train_file_name)
            if self.accuracy != None:
                text_infor = '- The accuracy of a SVM model is'+ str(' %.2f%%'%self.accuracy)+\
                             '\n- The prediction of classes saved in the OUTPUT folder!'
                tk.messagebox.showinfo('Save Prediction',text_infor)
            else:
                text_infor = 'The prediction of classes saved in the OUTPUT folder!'
                tk.messagebox.showinfo('Save Prediction',text_infor)
        except:
            tk.messagebox.showwarning('Error!!!', 'Did not input training or test file')
    # predict_save_file
    def save_classification(self):
        try:
            tk.messagebox.showinfo('Save Prediction','Done!!')
        except:
            tk.messagebox.showwarning('Error!!!', 'Did not input training or test file')

    # analyze prediction on test set
    def analyze_result(self):
        try:
            tcp=TextClassificationPredict()
            value=tcp.proccess_result()
            #labels = 'Phương pháp giảng dạy', 'Thái độ GV', 'Cơ sở vật chất', 'Ý kiến khác'
            text_infor = '- Class "Phương pháp giảng dạy": ' + str(value[0]) + '%' \
                         '\n - Class "Thái độ GV": ' + str(value[1]) + '%' + \
                         '\n - Class "Cơ sở vật chất": ' + str(value[2]) + '%' + \
                         '\n - Class "Ý kiến khác": ' + str(value[3]) + '%'
            tk.messagebox.showinfo('Analyze Results', text_infor)
        except:
            tk.messagebox.showwarning('Error!','Please select ''Predict text file'' first!')

    def populate_list(self):
        # Delete items before update. So when you keep pressing it doesnt keep getting (show example by calling this twice)
        self.parts_list.delete(0, tk.END)
        # Loop through records
        for row in db.fetch():
            # Insert into list
            self.parts_list.insert(tk.END, row)

    # Add new item
    def add_item(self):
        if self.input_exam_text.get() == '':
            messagebox.showerror("Required Fields", "Please include all fields")
            return
        print(self.input_exam_text.get())
        new_sentence=pd.DataFrame()
        new_sentence['test']=[self.input_exam_text.get()]
        # new_sentence['label'] = ['']
        tcp = TextClassificationPredict()
        self.data_of_testset = tcp.get_test_data(new_sentence)
        self.one_label=tcp.process_example(self.data_of_trainset,self.data_of_testset, self.train_file_name)
        # Insert into DB
        db.insert(self.input_exam_text.get(), "=>", self.one_label)
        # Clear list
        self.parts_list.delete(0, tk.END)
        # Insert into list
        self.parts_list.insert(tk.END, (self.input_exam_text.get(), "=>", self.one_label))
        self.clear_text()
        self.populate_list()

    # Runs when item is selected
    def select_item(self, event):
        # # Create global selected item to use in other functions
        # global self.selected_item
        try:
            # Get index
            index = self.parts_list.curselection()[0]
            # Get selected item
            self.selected_item = self.parts_list.get(index)
            # print(selected_item) # Print tuple

            # Add text to entries
            self.input_exam_entry.delete(0, tk.END)
            self.input_exam_entry.insert(tk.END, self.selected_item[1])
        except IndexError:
            pass

    # Remove item
    def remove_item(self):
        db.remove(self.selected_item[0])
        self.clear_text()
        self.populate_list()

    # Update item
    def update_item(self):
        db.update(self.selected_item[0], self.input_exam_text.get(), '=>', self.one_label)
        self.populate_list()

    # Clear all text fields
    def clear_text(self):
        self.input_exam_entry.delete(0, tk.END)

if __name__ == '__main__':
    root = tk.Tk()
    # root.iconbitmap('')
    # root.iconphoto(False, tk.PhotoImage(file='/home/trieunh/NTU/Tap_chi_Da_Lat/classification_project/logo/logo.png'))
    app = Application(master=root)
    app.mainloop()